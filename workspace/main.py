from root.utils.parameters import SERVICES
from root.optimizer import auto_best
from root.mlp.training import kfold, holdout, accuracy_evolution_by_data_size
import tensorflow as tf
from root.mlp.scores import compute_training_scores, best_model
from root.mlp.utils import get_model_info, make_optimization_folder, make_folders
from root.mlp.models import get_model

# best_model_name = best_model('stats.csv', 'valid')
# info = get_model_info(best_model_name)
# model = get_model(info)

# print(best_model_name)
auto_best('stats.csv', architectures=['standard0', 'standard1'], batch_sizes=[8, 16, 32, 64], optimizers = ['Adam', 'SGD'], learning_rates=[0.001, 0.002, 0.005, 0.01])
accuracy_evolution_by_data_size(sizes=[25, 50, 75, 100, 125, 150, 175, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 1000, 1200, 1400, 1600])
# new_model = tf.keras.models.load_model('my_model.h5')
# More ideas to explore:
# s = 'GitHub'
# kfold(s, stats_file=s+'_kfold.csv', epochs= 150, k= 10, plot_model= True)
# holdout(s, architecture= 'standard1', stats_file=s+'_holdout.csv', patience= 10)
