from root.utils.parameters import STATS_PATH
from root.mlp.parameters import STATS_COLUMNS
from root.utils.utils import check_csv

import os
import pandas as pd

def append_to_stats(stats, stats_csv_filename):
    check_csv(stats_csv_filename)
    if not os.path.exists(STATS_PATH + stats_csv_filename):
        stats_df = pd.DataFrame(columns= STATS_COLUMNS)
    else:
        stats_df = pd.read_csv(STATS_PATH + stats_csv_filename)

    stats_df = stats_df.append(stats, ignore_index=True)
    stats_df.to_csv(STATS_PATH + stats_csv_filename, index=False)

