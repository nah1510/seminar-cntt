from root.utils.parameters import TARGET
from root.utils.types import CATEGORICALS, NUMERICS
from root.mlp.architectures import ARCHITECTURES

import numpy as np
import tensorflow as tf
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Dense, Dropout, Input, concatenate
from tensorflow.keras.models import Model, Sequential

def get_model(info):

    model = Sequential()
    layers = ARCHITECTURES[info['architecture']].copy()
    first_layer = layers.pop(0)

    model.add(Dense(first_layer['units'], activation=first_layer['activation'], kernel_regularizer= regularizers.l1_l2(l1=first_layer['l1'], l2=first_layer['l2']), input_shape=(info['n_columns'], )))

    for layer in layers:
        model.add(Dense(layer['units'], activation=layer['activation'], kernel_regularizer= regularizers.l1_l2(l1=layer['l1'], l2=layer['l2'])))
        if 'dropout' in layer.keys():
            model.add(Dropout(layer['dropout']))

    model.add(Dense(1,  activation='sigmoid'))

    model.compile(optimizer=info['optimizer'], loss='binary_crossentropy', metrics=['accuracy', 'AUC'])
    model.summary()

    return model
