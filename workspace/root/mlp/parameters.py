##      MLP PARAMETERS:
BATCH_SIZE  = 8
EPOCHS      = 100
K           = 5
TEST_FRAC   = 0.2

METRICS = ['accuracy', 'loss', 'auc']

STATS_COLUMNS = [
    'service',
    'architecture',
    'optimizer',
    'learning_rate',
    'momentum',
    'mode',
    'n_train',
    'n_test',
    'n_columns',
    'columns',
    'batch_size',
    'epochs',
    'k',
    'callbacks',
    'monitors',
    'patience',
    'model_name',
    'save_path',

    'train_auc',
    'train_accuracy',
    'train_loss',

    'valid_auc',
    'valid_accuracy',
    'valid_loss',

    'test_auc',
    'test_accuracy',
    'test_loss'
]
