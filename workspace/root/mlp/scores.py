from root.utils.parameters import STATS_PATH, SERVICES
from root.utils.utils import check_service, check_csv

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def best_model(stats_file, mode):
    stats = pd.read_csv(STATS_PATH + stats_file)
    stats = stats[stats[mode + '_accuracy'].notna()]
    accuracy_scores = {np.mean(stats[stats['model_name'] == m][mode+'_accuracy']): m for m in set(stats['model_name'].values)}
    best_model = accuracy_scores[max(accuracy_scores.keys())]
    print('\nThe best model is: {}'.format(best_model))
    return best_model

def compute_training_scores(histories, metrics):
    max_epoch = max([len(h[metrics[0]]) for h in histories])
    for h in histories:
        for m in metrics:
            for val in ['', 'val_']:
                m = val + m
                h[m] = pad_history(h[m], max_epoch)

    out= {}
    mean_per_epoch = {}
    training_scores = {}
    
    for m in metrics:
        for val in ['', 'val_']:
            m = val + m

            mean = []
            for epoch in range(max_epoch):
                for h in histories:
                    mean.append(h[m][epoch])
            mean_per_epoch[m] = np.mean(mean)

            if m=='loss' or m=='val_loss':
                training_scores[m]              = np.min(mean_per_epoch[m])
                training_scores[m + 'epoch']    = np.argmin(mean_per_epoch[m])
            else:
                training_scores[m]              = np.max(mean_per_epoch[m])
                training_scores[m + 'epoch']    = np.argmax(mean_per_epoch[m])

    for m in metrics:
        out['train_' + m] = training_scores[m]
        out['valid_' + m] = training_scores['val_' + m]

    return out

def pad_history(l, width):
    content = l[-1]
    l.extend([content] * (width - len(l)))
    return l
