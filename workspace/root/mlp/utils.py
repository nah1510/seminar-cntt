from root.utils.parameters import SERVICES, MLP_PATH, AUTO_PATH
from root.utils.utils import make_directory

import numpy as np
import os

def make_folders():
    make_directory(MLP_PATH)
    make_directory(MLP_PATH+'stats/')
    make_directory(MLP_PATH+'plots/')
    make_directory(MLP_PATH+'architectures/')
    make_directory(MLP_PATH+'auto/')

def make_optimization_folder():
    i = 0
    while os.path.exists(AUTO_PATH + str(i)):
        i = i+1
    os.mkdir(AUTO_PATH + str(i))
    new_save_path = AUTO_PATH + str(i) + '/'
    return new_save_path

def print_info(training_info, folder):
    print('\n\nService: {}, Model: {}. K training: {}/{}\n\n'.format(training_info['service'], training_info['model_name'], folder+1, training_info['k']))

def get_model_info(model_name):
    architecture        = model_name.split('_B')[0]
    batch_size      = int(model_name.split('_B')[1].split('_O')[0])
    optimizer           = model_name.split('_O')[1].split('_L')[0]
    learning_rate = float(model_name.split('_L')[1].split('_M')[0])
    momentum      = float(model_name.split('_M')[1])

    model_info = {
        'architecture':   architecture,
        'batch_size':     batch_size,
        'optimizer':      optimizer,
        'learning_rate':  learning_rate,
        'momentum':       momentum,
        'model_name':     model_name,
    }

    return model_info
