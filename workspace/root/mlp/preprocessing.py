from root.utils.types import CATEGORICALS, STRINGS, NUMERICS, TYPES
from root.utils.parameters import TARGET
from sklearn.preprocessing import StandardScaler

import numpy as np
import pandas as pd
import tensorflow as tf

def one_hot_encoding(df, columns, dummy_value):
    columns = [c for c in columns if c in df.columns]
    for column in columns:
        df[column] = df[column].fillna(dummy_value)
        df = pd.concat([df.drop(column, axis=1), pd.get_dummies(df[column], prefix= column, prefix_sep='_is_')], axis=1)
        
    return df

def numeric_encoding(df, columns, dummy_value):
    columns = [c for c in columns if c in df.columns]
    if columns:
        for column in columns:
            new_column = column + '_aux'
            df[new_column] = df[column].notna().astype(float)
            df[column] = df[column].fillna(dummy_value)
        df[columns] = StandardScaler().fit_transform(df[columns])
    return df

def string_encoding(df, columns):
    columns = [c for c in columns if c in df.columns]
    for column in columns:
        new_column = column + '_aux'
        df[new_column] = df[column].notna().astype(float)
        df = df.drop(columns = [column])
        
    return df

def train_preprocessing(df, service):
    i = 0
    dummy_value = 'None' + str(i)
    while any(df.isin([dummy_value]).any()):
        i = i+1
        dummy_value = 'None' + str(i)

    if CATEGORICALS[service]:
        df = one_hot_encoding(df, CATEGORICALS[service], dummy_value)

    if STRINGS[service]:
        df = string_encoding(df, STRINGS[service])

    if NUMERICS[service]:
        df = numeric_encoding(df, NUMERICS[service], 0)
    return df

def train_test_preprocessing(train_df, test_df, service):
    train_index = range(len(train_df.index))
    test_index = range(len(train_df.index), len(train_df.index) + len(test_df.index))
    mixed_df = pd.concat([train_df, test_df])

    mixed_df = train_preprocessing(mixed_df, service)
    train_df = mixed_df.iloc[train_index, :]
    test_df  = mixed_df.iloc[test_index, :]

    return train_df, test_df
