from root.utils.parameters import SERVICES
from root.mlp.parameters import BATCH_SIZE, EPOCHS
from root.mlp.training import kfold, holdout
from root.mlp.utils import get_model_info, make_optimization_folder, make_folders
from root.mlp.stats import append_to_stats
from root.mlp.scores import best_model
from root.mlp.plots import plot_best_model_scores
from root.data import get_train_test_df

import os
import pandas as pd

def auto_best(stats_file='default.csv', architectures = ['standard0'], batch_sizes= [BATCH_SIZE], optimizers= ['Adam'], 
            learning_rates= [0.001], epochs= EPOCHS, plot_scores= True):
    make_folders()
    new_save_path = make_optimization_folder()

    print('Number of experiments per service: {}'.format(len(architectures)*len(batch_sizes)*len(optimizers)*len(learning_rates)))

    for s in SERVICES:
        train_df, test_df = get_train_test_df(s, test_frac= 0.2)
        train_df.to_csv(new_save_path + s + '_train.csv', index=False)
        test_df.to_csv( new_save_path + s + '_test.csv',  index=False)
        for a in architectures:
            for b in batch_sizes:
                for o in optimizers:
                    for l in learning_rates:
                        kfold(s, df=train_df, architecture=a, batch_size= b, optimizer= o, learning_rate= l, epochs= epochs, stats_file=stats_file)
    
    best_model_name = best_model(stats_file, 'valid')
    best_model_info = get_model_info(best_model_name)

    for s in SERVICES:
        train_df = pd.read_csv(new_save_path + s + '_train.csv')
        test_df  = pd.read_csv(new_save_path + s + '_test.csv')
        holdout(s, train_df=train_df, test_df=test_df, stats_file = stats_file,
            architecture= best_model_info['architecture'], batch_size= best_model_info['batch_size'], 
            optimizer= best_model_info['optimizer'], learning_rate= best_model_info['learning_rate'], 
            momentum= best_model_info['momentum'], epochs= EPOCHS, 
            plot_model= True, callbacks = ['early_stopping'], patience= 5)

    if plot_scores:
        plot_best_model_scores(stats_file)

    return best_model_name
