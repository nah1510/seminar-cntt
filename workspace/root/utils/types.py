import json

from root.utils.parameters import SERVICES

with open('../data/types.json') as f:
    TYPES = json.load(f)

CATEGORICALS = {}
STRINGS      = {}
NUMERICS     = {}

for s in SERVICES:
    CATEGORICALS[s] = []
    STRINGS[s]      = []
    NUMERICS[s]     = []

    for column, column_type in TYPES[s].items():
        if column_type == 'str':
            STRINGS[s].append(column)
        elif column_type == 'numeric':
            NUMERICS[s].append(column)
        elif column_type == 'category':
            CATEGORICALS[s].append(column)
        else:
            print('Something is wrong.. {}, {}, {}'.format(s, column, column_type))
