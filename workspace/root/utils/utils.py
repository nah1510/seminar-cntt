from root.utils.parameters import SERVICES

import os
import shutil

def make_directory(name):
    if not os.path.exists(name):
        os.mkdir(name)

def empty_directory(name):
    if os.path.exists(name):
        shutil.rmtree(name)
    os.mkdir(name)

def delete_file(name):
    if os.path.exists(name):
        os.remove(name)
    os.mkdir(name)

def pretty_dict(d, indent=0):
    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty_dict(value, indent+1)
        else:
            print('\t' * (indent+1) + str(value))

def check_service(service):
    if service not in SERVICES:
        raise ValueError('The service {} does not exists'.format(service))

def check_csv(stats_file):
    if stats_file[-4:] != '.csv':
        raise ValueError('{} is not a complete .csv filename'.format(stats_file))
