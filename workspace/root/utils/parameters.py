##      PATHS:
PREPROC_PATH       = '../data/input/'
 
MLP_PATH           = '../data/mlp/'
STATS_PATH         = '../data/mlp/stats/'
PLOTS_PATH         = '../data/mlp/plots/'
ARCHITECTURES_PATH = '../data/mlp/architectures/'
AUTO_PATH          = '../data/mlp/auto/'

TARGET = 'faulty'

SERVICES= [
    'LanguageTool',
    'StripeCoupon',
    'StripeProduct',
    'Yelp',
    'YouTubeCommentThreads',
    'YouTubeVideos',
    'YouTubeSearch'
]
