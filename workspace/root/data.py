from root.utils.parameters import PREPROC_PATH, SERVICES, TARGET
from root.mlp.parameters import TEST_FRAC

import pandas as pd
import numpy as np
import math
import os
import shutil
from sklearn.model_selection import train_test_split

def get_df(service, mode= 'train', na= False):

    df = pd.read_csv(PREPROC_PATH + service + '.csv')

    if mode == 'valid':
        df = df[df[TARGET] == 0]
    elif mode == 'faulty':
        df = df[df[TARGET] == 1]

    return df

def get_train_test_df(service, test_frac= TEST_FRAC, mode= 'train', na= False, n_sample= 'full'):
    df = get_df(service, mode= mode)

    if isinstance(n_sample, int):
        df = df.sample(n = n_sample)

    train_df, test_df = train_test_split(df, test_size=test_frac)

    return train_df, test_df
