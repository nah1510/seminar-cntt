model_name= 'standard0_B8_OAdam_L0.002_M0.0'

architecture        = model_name.split('_B')[0]
batch_size      = int(model_name.split('_B')[1].split('_O')[0])
optimizer           = model_name.split('_O')[1].split('_L')[0]
learning_rate = float(model_name.split('_L')[1].split('_M')[0])
momentum      = float(model_name.split('_M')[1])

model_info = {
    'architecture':   architecture,
    'batch_size':     batch_size,
    'optimizer':      optimizer,
    'learning_rate':  learning_rate,
    'momentum':       momentum,
    'model_name':     model_name,
}

print(model_info['optimizer'])
