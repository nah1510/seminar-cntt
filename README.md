
# Deep Learning-Based Prediction of Test Input Validity for RESTful APIs
### Anonymous repository for [DeepTest2021 Workshop](https://conf.researchr.org/home/deeptest-2021)

To reproduce study results:

* install `python>=3.7` and `pip`
* `cd workspace`
* `pip install -r ../requirements.txt`
* `python main.py`

Results will be saved into `data/mlp/stats` and `data/mlp/plots`, while the collected input dataset is located in `data/input/`.

Further configurations for both research questions (RQs) can be easily tested by:

* uncommenting examples functions in `main.py`
* changing hyperparameters under test in `auto_best(...)`
* exlopring new dataset sizes in `accuracy_evolution_by_data_size(...)`


